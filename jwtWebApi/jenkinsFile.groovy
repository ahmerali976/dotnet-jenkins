pipeline {
    agent any
    
    stages {
        stage('Clone') {
            steps {
                git branch: 'main', credentialsId: '47462002', url: 'https://gitlab.com/ahmerali976/dotnet-jenkins.git'
            }
        }
        
        stage('Restore Packages') {
            steps {
                sh 'dotnet restore'
            }
        }
        
        stage('Build') {
            steps {
                sh 'dotnet build'
            }
        }
        
        stage('Unit Test') {
            steps {
                sh 'dotnet test'
            }
        }
        
        stage('Publish') {
            steps {
                sh 'dotnet publish --output ./publish'
            }
        }
    }
}
